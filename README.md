# CreativeGUI #

Here lives a beautiful, elegant, minimal drop-in dark GUI replacement for Minecraft 1.8 developed with love for CreativeCraft.

## Screenshots ##

![day.jpg](https://bitbucket.org/repo/xxR74p/images/638172068-day.jpg)
![search.jpg](https://bitbucket.org/repo/xxR74p/images/2351622852-search.jpg)
![menu.jpg](https://bitbucket.org/repo/xxR74p/images/1673366816-menu.jpg)